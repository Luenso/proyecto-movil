package proyectomovil.facci.eventplan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class CrearEventoActivity extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener{
    Button btGuardarEvento, btLugar, btCrearInvi, btPublicar, btReservar;
    EditText nombre, lugar, tipoEvento, fecha;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_evento);
        btCrearInvi = findViewById(R.id.btCrearInvi);
        btLugar = findViewById(R.id.btLugar);
        btPublicar = findViewById(R.id.btPublicar);
        btReservar = findViewById(R.id.btReservar);
        btGuardarEvento = findViewById(R.id.GuardarEvento);
        nombre = findViewById(R.id.nombreEventoRe);
        lugar = findViewById(R.id.LugarEventoRe);
        tipoEvento = findViewById(R.id.TipoEventoRe);
        fecha = findViewById(R.id.FechaEventoRe);
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        btLugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrearEventoActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });

        btGuardarEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nombre.getText().toString().equals("") || lugar.getText().toString() == "" || tipoEvento.getText().toString() == ""
                        || fecha.getText().toString() == ""){

                    Toast.makeText(getApplicationContext(),"Ingrese todos los campos", Toast.LENGTH_SHORT).show();
                } else {
                    cargarWebServices();
                    Intent intent = new Intent(CrearEventoActivity.this, MenuActivity.class);
                    startActivity(intent);
                }
            }
        });


    }

    private void cargarWebServices() {
        String url = "http://10.22.26.245/eventplan/epJSONRegistroEvento.php?nombre="+nombre.getText().toString()+
                "&lugar="+lugar.getText().toString()+
                "&tipo_evento="+tipoEvento.getText().toString()+
                "&fecha="+fecha.getText().toString();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        requestQueue.add(jsonObjectRequest);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "No se pudo registrar "+error.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(getApplicationContext(),"Se ha registrado correctamente", Toast.LENGTH_SHORT).show();
        nombre.setText("");
        lugar.setText("");
        tipoEvento.setText("");
        fecha.setText("");

    }
}
