package proyectomovil.facci.eventplan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.nio.file.Files;

public class RegistroActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener{
    EditText nombres, apellidos, celular, email, contraseña, recontraseña;
    Button registrar;
    ProgressBar progressBar;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        nombres = findViewById(R.id.etNombres);
        apellidos = findViewById(R.id.etApellidos);
        celular = findViewById(R.id.etCelular);
        email = findViewById(R.id.etEmail);
        contraseña = findViewById(R.id.etContraseña);
        recontraseña = findViewById(R.id.etRepiContraseña);
        registrar = findViewById(R.id.btGuardarRegis);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nombres.getText().toString().equals("") || apellidos.getText().toString() == "" || celular.getText().toString() == ""
                        || email.getText().toString() == "" || contraseña.getText().toString() == ""|| recontraseña.getText().toString() == ""){

                    Toast.makeText(getApplicationContext(),"Ingrese todos los campos", Toast.LENGTH_SHORT).show();
                } else {
                    cargarWebService();
                }
            }
        });

    }

    private void cargarWebService() {

            String url = "http://10.22.26.245/eventplan/epJSONRegistro.php?nombres="+nombres.getText().toString()+
                    "&apellidos="+apellidos.getText().toString()+
                    "&celular="+celular.getText().toString()+
                    "&email="+email.getText().toString()+"&contrasena="+contraseña.getText().toString();
            url = url.replace(" ", "%20");
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            requestQueue.add(jsonObjectRequest);


    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "No se pudo registrar "+error.toString(), Toast.LENGTH_SHORT).show();
        Log.i("ERROR", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(getApplicationContext(),"Se ha registrado correctamente", Toast.LENGTH_SHORT).show();
        nombres.setText("");
        apellidos.setText("");
        celular.setText("");
        email.setText("");
        contraseña.setText("");
        recontraseña.setText("");
        Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
