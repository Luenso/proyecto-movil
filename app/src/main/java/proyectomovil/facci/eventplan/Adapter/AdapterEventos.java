package proyectomovil.facci.eventplan.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;


import java.util.List;


import proyectomovil.facci.eventplan.Entidades.Eventos;
import proyectomovil.facci.eventplan.R;

public class AdapterEventos extends RecyclerView.Adapter<AdapterEventos.ViewHolderEventos> {

    List<Eventos> listEventos;
    public AdapterEventos(List<Eventos> listEventos){
        this.listEventos = listEventos;
    }

    @NonNull
    @Override
    public ViewHolderEventos onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view =LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_evento, null, false);
        RecyclerView.LayoutParams layoutParams= new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(layoutParams);
        return new ViewHolderEventos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderEventos holder, int position) {
        holder.nombreEvento.setText(listEventos.get(position).getNombre());
        holder.lugarEvento.setText(listEventos.get(position).getLugar());
        holder.tipoEvento.setText(listEventos.get(position).getTipoEvento());
        holder.fechaEvento.setText(listEventos.get(position).getFecha().toString());
    }

    @Override
    public int getItemCount() {
        return listEventos.size();
    }

    public class ViewHolderEventos extends RecyclerView.ViewHolder {
        TextView nombreEvento, lugarEvento, tipoEvento, fechaEvento;
        public ViewHolderEventos(@NonNull View itemView) {
            super(itemView);
            nombreEvento = itemView.findViewById(R.id.nombreEvento);
            lugarEvento = itemView.findViewById(R.id.LugarEvento);
            tipoEvento = itemView.findViewById(R.id.TipoEvento);
            fechaEvento = itemView.findViewById(R.id.FechaEvento);
        }


    }
}
