package proyectomovil.facci.eventplan;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import proyectomovil.facci.eventplan.Adapter.AdapterEventos;
import proyectomovil.facci.eventplan.Entidades.Eventos;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Response.Listener<JSONObject>, Response.ErrorListener{
    TextView Nombres, Email;
    Button buttonCamara;
    //ImageView imageViewFoto;
    Bitmap imageBitmap;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;
    Eventos eventos = new Eventos();
    RecyclerView recyclerViewEventos;
    ArrayList<Eventos> lisEventos;
    private  final int REQUEST_PERMISSION_SAVE = 101;
    static final int REQUEST_IMAGE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Nombres = findViewById(R.id.textViewNombres);
        Email = findViewById(R.id.textViewEmail);

        //String nombres = getIntent().getStringExtra("nombres");
        //String email = getIntent().getStringExtra("email");
        //Nombres.setText(nombres);
        //Email.setText(email);
        //imageViewFoto = (ImageView)findViewById(R.id.imageViewFoto);
        //CargarInfo();

        lisEventos = new ArrayList<>();

        recyclerViewEventos = findViewById(R.id.RecyclerEventos);

        recyclerViewEventos.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));
        recyclerViewEventos.setHasFixedSize(true);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        CargarWebServices();

        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(MenuActivity.this, CrearEventoActivity.class);
               startActivity(intent);
               finish();
                // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });


        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea cerrar sesión?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialogo1, int id) {
                    Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                    startActivity(intent);
                    MenuActivity.super.onBackPressed();
                }
            });
            dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {

                }
            });
            dialogo1.show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_crearEvento) {
            Intent intent = new Intent(MenuActivity.this, CrearEventoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_servicio) {
            Intent intent = new Intent(MenuActivity.this, ServiciosActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_ayuda) {


        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode ==  REQUEST_IMAGE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            //Asignar al objeto de tipo bitmap lo que me devuelve el objeto data.
            imageBitmap = (Bitmap) extras.get("data");
            //imageViewFoto.setImageBitmap(imageBitmap);

        }
    }



    public void CargarInfo(){
        String nombres = getIntent().getStringExtra("nombres");
        String email = getIntent().getStringExtra("email");
        Nombres.setText(nombres);
        Email.setText(email);
    }

    private void CargarWebServices() {
        String url = "http://10.22.26.245/eventplan/epJSONListaEventos.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        requestQueue.add(jsonObjectRequest);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "No se puede conectar "+error.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {

        Eventos eventos = null;

        JSONArray jsonArray = response.optJSONArray("eventos");
        try {

            for (int i=0; i<jsonArray.length(); i++){
            eventos=new Eventos();
            JSONObject jsonObject =null;
            jsonObject = jsonArray.getJSONObject(i);

            eventos.setNombre(jsonObject.optString("nombre"));
            eventos.setLugar(jsonObject.optString("lugar"));
            eventos.setTipoEvento(jsonObject.optString("tipo_evento"));
            eventos.setFecha(jsonObject.optString("fecha"));
            lisEventos.add(eventos);
            }
            AdapterEventos adapterEventos = new AdapterEventos(lisEventos);
            recyclerViewEventos.setAdapter(adapterEventos);
        }catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexion" + response,
                    Toast.LENGTH_LONG).show();
        }
    }


}

