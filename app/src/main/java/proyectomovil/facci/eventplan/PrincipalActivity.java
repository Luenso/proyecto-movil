package proyectomovil.facci.eventplan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PrincipalActivity extends AppCompatActivity {
Button buttonInvitado, buttonRegistrado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        buttonInvitado = findViewById(R.id.buttonInvitado);
         buttonRegistrado = findViewById(R.id.buttonRegistrado);

         buttonInvitado.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(PrincipalActivity.this, InvitadoActivity.class);
                 startActivity(intent);

             }
         });
         buttonRegistrado.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(PrincipalActivity.this, MainActivity.class);
                 startActivity(intent);

             }
         });
    }
}
