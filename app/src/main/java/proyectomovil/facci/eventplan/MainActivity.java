package proyectomovil.facci.eventplan;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import proyectomovil.facci.eventplan.Entidades.Cliente;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    Button buttonIngresar, buttonRegistrar;
    EditText etemail, etcontrasena;
    TextView txterror;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;
    Cliente cliente = new Cliente();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonIngresar = findViewById(R.id.buttonIngresar);
        buttonRegistrar = findViewById(R.id.buttonRegistrate);
        etemail = findViewById(R.id.editTextemail);
        etcontrasena  =findViewById(R.id.editTextcontrasena);
        txterror = findViewById(R.id.TextError);
        requestQueue = Volley.newRequestQueue(getApplicationContext());





        buttonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etemail.getText().toString().equals("") ||
                        etcontrasena.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Ingrese todos los campos",Toast.LENGTH_SHORT).show();
                }else {
                cargarWebService();
                //verificacionCliente();
                }
                //Intent intent = new Intent(MainActivity.this, MenuActivity.class);
               // startActivity(intent);
            }
        });

        buttonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,  RegistroActivity.class);
                startActivity(intent);
            }
        });

    }

    private void cargarWebService() {


        String url = "http://10.22.26.245/eventplan/epJSONConsulta.php?email="+etemail.getText().toString()+
                "&contrasena="+etcontrasena.getText().toString();
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        requestQueue.add(jsonObjectRequest);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "No está registrado"+error.toString(), Toast.LENGTH_SHORT).show();
        txterror.setText("Email o contraseña no son correctos");

    }



    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(getApplicationContext(),"Mensaje"+response, Toast.LENGTH_SHORT).show();



        JSONArray jsonArray = response.optJSONArray("cliente");
        JSONObject jsonObject;

        try {
            jsonObject = jsonArray.getJSONObject(0);
            //cliente.setNombres(jsonObject.getString("nombres"));
            //cliente.setApellidos(jsonObject.getString("apellidos"));
            cliente.setEmail(jsonObject.getString("email"));
            cliente.setContrasena(jsonObject.getString("contrasena"));
            //String nombres = cliente.getNombres() +" "+cliente.getApellidos();
            String email = cliente.getEmail();
            String contrasena = cliente.getContrasena();

            if (!etcontrasena.getText().toString().equals(contrasena)
                    || !etemail.getText().toString().equals(email)){
                txterror.setText("Email o contraseña no son correctos");
            }else {
                Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                //intent.putExtra("nombres", nombres);
                //intent.putExtra("email", email);
                startActivity(intent);
                txterror.setText("");
                finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }




    }



}
