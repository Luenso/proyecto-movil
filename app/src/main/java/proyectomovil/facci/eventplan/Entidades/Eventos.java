package proyectomovil.facci.eventplan.Entidades;

import java.util.Date;

public class Eventos {
    private String nombre;
    private String lugar;
    private String tipoEvento;
    private String fecha;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(String tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha (String fecha) {
        this.fecha = fecha;
    }


}
